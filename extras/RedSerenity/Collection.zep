namespace RedSerenity;

use ArrayIterator;

class Collection implements CollectionInterface {

	protected _Data = [];

	public function __construct(array Data = null) {
		if (!empty Data) {
			let this->_Data = Data;
		}
	}

	/* Collection Interface */

	public function Set(var Key, var Value) -> void {
		if (is_null(Key) && is_array(Value)) {
			let this->_Data = Value;
		} else {
			let this->_Data[Key] = Value;
		}
	}

	public function Get(var Key, var ValueDefault = null) -> var {
		return this->Has(Key) ? this->_Data[Key] : ValueDefault;
	}

	public function Keys() -> array {
		return array_keys(this->_Data);
	}
  public function Values() -> array {
  	return array_values(this->_Data);
  }

	public function Has(var Key) -> bool {
		return array_key_exists(Key, this->_Data);
	}

	public function Delete(var Key) -> void {
		unset(this->_Data[Key]);
	}

	public function Clear() -> void {
		let this->_Data = [];
	}

	public function __toString() -> string {
		return str_replace("=", ":", urldecode(http_build_query(this->_Data, null, ", ")));
	}

	public function __toArray() -> array {
		return this->_Data;
	}

	public function __toJson() -> string {
		return json_encode(this);
	}

	/* JsonSerializable Interface */

	public function jsonSerialize() -> var {
		return this->_Data;
	}

	/* ArrayAccess Interface */

	public function offsetExists(var Key) {
		return this->Has(Key);
	}

	public function offsetGet(var Key) -> var {
		return this->Get(Key);
	}

	public function offsetSet(var Key, var Value) -> void {
		this->Set(Key, Value);
	}

	public function offsetUnset(var Key) -> void {
		this->Delete(Key);
	}

	/* Countable Interface */

	public function count() -> int {
		return count(this->_Data);
	}

	/* IteratorAggregate Interface */

	public function getIterator() -> <ArrayIterator> {
		return new ArrayIterator(this->_Data);
	}

}