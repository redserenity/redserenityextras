namespace RedSerenity;

interface CollectionInterface extends \ArrayAccess, \Countable, \IteratorAggregate, \JsonSerializable {

	public function Set(var Key, var Value) -> void;
	public function Get(var Key, var ValueDefault = null) -> var;
	public function Keys() -> array;
	public function Values() -> array;
	public function Has(var Key) -> bool;
	public function Delete(var Key) -> void;
	public function Clear() -> void;

	public function __toString() -> string;
	public function __toArray() -> array;
	public function __toJson() -> string;

}