<?php

namespace RedSerenity;


interface CollectionInterface extends \ArrayAccess, \Countable, \IteratorAggregate, \JsonSerializable
{

    /**
     * @param mixed $Key
     * @param mixed $Value
     */
    public function Set($Key, $Value);

    /**
     * @param mixed $Key
     * @param mixed $ValueDefault
     * @return mixed
     */
    public function Get($Key, $ValueDefault = null);

    /**
     * @return array
     */
    public function Keys();

    /**
     * @return array
     */
    public function Values();

    /**
     * @param mixed $Key
     * @return bool
     */
    public function Has($Key);

    /**
     * @param mixed $Key
     */
    public function Delete($Key);


    public function Clear();

    /**
     * @return string
     */
    public function __toString();

    /**
     * @return array
     */
    public function __toArray();

    /**
     * @return string
     */
    public function __toJson();

}
