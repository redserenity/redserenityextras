<?php

namespace RedSerenity;


class Collection implements \RedSerenity\CollectionInterface
{

    protected $_Data = array();


    /**
     * @param array $Data
     */
    public function __construct(array $Data = null) {}

    /**
     * @param mixed $Key
     * @param mixed $Value
     */
    public function Set($Key, $Value) {}

    /**
     * @param mixed $Key
     * @param mixed $ValueDefault
     * @return mixed
     */
    public function Get($Key, $ValueDefault = null) {}

    /**
     * @return array
     */
    public function Keys() {}

    /**
     * @return array
     */
    public function Values() {}

    /**
     * @param mixed $Key
     * @return bool
     */
    public function Has($Key) {}

    /**
     * @param mixed $Key
     */
    public function Delete($Key) {}


    public function Clear() {}

    /**
     * @return string
     */
    public function __toString() {}

    /**
     * @return array
     */
    public function __toArray() {}

    /**
     * @return string
     */
    public function __toJson() {}

    /**
     * @return mixed
     */
    public function jsonSerialize() {}

    /**
     * @param mixed $Key
     */
    public function offsetExists($Key) {}

    /**
     * @param mixed $Key
     * @return mixed
     */
    public function offsetGet($Key) {}

    /**
     * @param mixed $Key
     * @param mixed $Value
     */
    public function offsetSet($Key, $Value) {}

    /**
     * @param mixed $Key
     */
    public function offsetUnset($Key) {}

    /**
     * @return int
     */
    public function count() {}

    /**
     * @return \ArrayIterator
     */
    public function getIterator() {}

}
